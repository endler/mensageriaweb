DROP VIEW  v_relatorio_mensagens_categoria_hora cascade;
 

CREATE OR REPLACE VIEW public.v_relatorio_mensagens_categoria_hora AS 
select 
	horas.hour,
	cat.descricao as categoria,
	(select count(*) from mensagens where date_trunc('hour', timestamp) = horas.hour and id_categoria = cat.id_categoria) as qtd
from 
 (SELECT 
    date_trunc('hour', mensagens."timestamp") AS hour
   FROM mensagens
     GROUP BY 1
  ) horas,
  ( SELECT descricao, id_categoria from categorias where id_categoria in (select id_categoria from mensagens) group by 1, 2) cat






DROP VIEW public.v_relatorio_mensagens_hora;


CREATE OR REPLACE VIEW public.v_relatorio_mensagens_hora AS 
 SELECT date_trunc('hour'::text, mensagens."timestamp") AS hour,
    'Total '::text AS categoria,
    count(*) AS count
   FROM mensagens
  GROUP BY (date_trunc('hour'::text, mensagens."timestamp"))
  ORDER BY (date_trunc('hour'::text, mensagens."timestamp"));

ALTER TABLE public.v_relatorio_mensagens_hora
  OWNER TO postgres;





CREATE OR REPLACE VIEW public.v_relatorio_mensagens AS 
 SELECT * from v_relatorio_mensagens_hora
UNION
 SELECT * FROM v_relatorio_mensagens_categoria_hora
  ORDER BY 1, 2;

