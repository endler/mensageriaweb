package br.pucrio.inf.mensageriaweb.domain;

/**
 * Enum que define o status de uma Regra.
 * 
 * @author Felipe
 *
 */
public enum SituacaoRegra {
    ativa, inativa;

    /**
     * @return Situa��o inversa da regra para mudan�a no banco de dados. 
     */
    public SituacaoRegra reverse() {
        if (this == ativa) {
            return inativa;
        } else {
            return ativa;
        }
    }
}
