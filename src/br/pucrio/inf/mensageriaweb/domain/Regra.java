package br.pucrio.inf.mensageriaweb.domain;

import br.pucrio.inf.mensageriaweb.database.ConnectionSGBD;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Classe que representa uma Regra (Filtro) contendo as seguintes informa��es:
 * <ul>
 * <li>id: ID da regra</li>
 * <li>epl: Comando de sele��o da regra em linguagem EPL(Event Processing Language)</li>
 * <li>name: Nome da regra</li>
 * <li>status: Situa��o da regra (ativa ou inativa)</li> 
 * </ul>
 * 
 * @see <a href="http://www.espertech.com/esper/release-5.3.0/esper-reference/html/epl_clauses.html">Guia de refer�ncia EPL</a>
 * 
 * @author Felipe
 *
 */
public class Regra {

    private int id;
    private String epl;
    private String name;
    private SituacaoRegra status;

    public Regra(int id, String epl, String name, SituacaoRegra status) {
        this.id = id;
        this.epl = epl;
        this.name = name;
        this.status = status;
    }
    public Regra(int id, String name, String epl) {
        this.id = id;
        this.epl = epl;
        this.name = name;
    }
    
    public Regra(String name, String epl) {
        this.epl = epl;
        this.name = name;
    }
    public Regra(int id) {
    	this.id = id;
    }
    public Regra(int id, SituacaoRegra status) {
    	this.id = id;
    	this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEpl() {
        return epl;
    }

    public void setEpl(String epl) {
        this.epl = epl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SituacaoRegra getStatus() {
        return status;
    }

    public void setStatus(SituacaoRegra status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Regra)) {
            return false;
        }
        if (obj == this) {
            return true;
        }

        Regra filtro = (Regra) obj;
        return new EqualsBuilder().
                append(id, filtro.id).
                append(epl, filtro.epl).
                append(status, filtro.status).
                append(name, filtro.name).
                isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31).
                append(id).
                append(epl).
                append(status).
                append(name).
                toHashCode();
    }

    /**
     * M�todo para editar ou inserir uma regra. Se a regra n�o possui ID ela � nova, ent�o ser� salva no banco de dados.
     * Caso a regra j� possua um ID, ela ser� editada no banco de dados.
     */
    public void save() {
        if (this.getId() <= 0) {
            this.insert();
        } else {
            this.edit();
        }
    }

    /**
     * M�todo para inserir uma nova regra no banco de dados com as informa��es espec�ficas.
     */
    private void insert() {
        try {
            ConnectionSGBD conn = ConnectionSGBD.getInstance();
            PreparedStatement pps = conn.getConnection().prepareStatement("insert into regras (regra, nome, situacao) values (?, ?, ?)");
            pps.setString(1, getEpl());
            pps.setString(2, getName());
            pps.setString(3, "ativa");
            pps.execute();
        } catch (SQLException ex) {
            Logger.getLogger(Regra.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * M�todo para editar uma regra j� existente no banco de dados.
     */
    private void edit() {
        try {
            ConnectionSGBD conn = ConnectionSGBD.getInstance();
            PreparedStatement pps = conn.getConnection().prepareStatement("update regras set regra = ?, nome = ? where id_regra = ?");
            pps.setString(1, getEpl());
            pps.setString(2, getName());
            pps.setInt(3, getId());
            pps.execute();
        } catch (SQLException ex) {
            Logger.getLogger(Regra.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * M�todo para excluir uma regra do banco de dados.
     */
    public void delete() {
        try {
            ConnectionSGBD conn = ConnectionSGBD.getInstance();
            PreparedStatement pps = conn.getConnection().prepareStatement("delete from regras where id_regra = ?");
            pps.setInt(1, getId());
            pps.execute();
        } catch (SQLException ex) {
            Logger.getLogger(Regra.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * M�todo para inverter a situa��o de uma regra no banco de dados (ativa ou inativa).
     */
    public void changeStatus() {
        try {
            ConnectionSGBD conn = ConnectionSGBD.getInstance();
            PreparedStatement pps = conn.getConnection().prepareStatement("update regras set situacao = '" + status.reverse() + "' where id_regra = ?");
            pps.setInt(1, getId());
            pps.execute();
        } catch (SQLException ex) {
            Logger.getLogger(Regra.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
