package br.pucrio.inf.mensageriaweb.domain;

import java.sql.Timestamp;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Esta classe estende {@link Mensagem} relacionando-a a um filtro.
 * Este filtro � o filtro espec�fico que filtrou a mensagem para a sa�da.
 * @author Felipe
 *
 */
public class MensagemFiltrada extends Mensagem {
	
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	private Regra filtro;	
    protected String criticity;
    protected String poi;

    public Regra getFiltro() {
        return filtro;
    }

    public void setFiltro(Regra filtro) {
        this.filtro = filtro;
    }    
    
    public String getCriticity() {
		return criticity;
	}

	public void setCriticity(String criticity) {
		this.criticity = criticity;
	}

	public String getPoi() {
		return poi;
	}

	public void setPoi(String poi) {
		this.poi = poi;
	}

	@Override
    public boolean equals(Object obj) {
    	if (!(obj instanceof MensagemFiltrada))
            return false;
        if (obj == this)
            return true;

        MensagemFiltrada msg = (MensagemFiltrada) obj;
        return new EqualsBuilder().
            append(id, msg.id).
            append(categoryID, msg.categoryID).
            append(filtro, msg.filtro).
            append(poi, msg.poi).
            isEquals();
    }
    
    @Override
    public int hashCode() {
    	return new HashCodeBuilder(17, 31).
                append(id).
                append(categoryID).
                append(filtro).
                append(poi).
                toHashCode();
    }

    //////////////////////////////////////////////////////////////////////////
    // Builder
    //////////////////////////////////////////////////////////////////////////
    public static class Builder {

        private int id;
        private int categoryID;
        private String description;
        private String criticity;
        private Timestamp timestamp;
        private String icon;
        private String title;
        private double latitude;
        private double longitude;
        private String category;
        private String situation;
        private Regra filtro;
        private String poi;

        public Builder() {
        }

        public Builder id(final int value) {
            this.id = value;
            return this;
        }

        public Builder categoryID(final int value) {
            this.categoryID = value;
            return this;
        }

        public Builder description(final String value) {
            this.description = value;
            return this;
        }

        public Builder criticity(final String value) {
            this.criticity = value;
            return this;
        }

        public Builder timestamp(final Timestamp value) {
            this.timestamp = value;
            return this;
        }

        public Builder icon(final String value) {
            this.icon = value;
            return this;
        }

        public Builder title(final String value) {
            this.title = value;
            return this;
        }

        public Builder latitude(final double value) {
            this.latitude = value;
            return this;
        }

        public Builder longitude(final double value) {
            this.longitude = value;
            return this;
        }

        public Builder category(final String value) {
            this.category = value;
            return this;
        }

        public Builder situation(final String value) {
            this.situation = value;
            return this;
        }

        public Builder filtro(final Regra value) {
            this.filtro = value;
            return this;
        }

        public Builder poi(final String ppoi) {
            if (ppoi != null && !ppoi.isEmpty()) {
                this.poi = ppoi;
            } else {
                this.poi = "";
            }
            return this;
        }

        public MensagemFiltrada build() {
            return new MensagemFiltrada(id, categoryID, description, criticity, timestamp, icon, title, latitude, longitude, category, situation, filtro, poi);
        }
    }

    public static MensagemFiltrada.Builder builder() {
        return new MensagemFiltrada.Builder();
    }

    private MensagemFiltrada(final int id, final int categoryID, final String description, final String criticity, final Timestamp timestamp, final String icon, final String title, final double latitude, final double longitude, final String category, final String situation, final Regra filtro, final String poi) {
        this.id = id;
        this.categoryID = categoryID;
        this.description = description;
        this.criticity = criticity;
        this.timestamp = timestamp;
        this.icon = icon;
        this.title = title;
        this.latitude = latitude;
        this.longitude = longitude;
        this.category = category;
        this.situation = situation;
        this.filtro = filtro;
        this.poi = poi;
    }
}
