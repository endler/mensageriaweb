package br.pucrio.inf.mensageriaweb.domain;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.apache.log4j.Logger;

/**
 * Esta classe � respons�vel pela busca de um arquivo de propriedades
 * armazenado em disco.
 * 
 * <p>O arquivo de propriedades cont�m os seguintes par�metros de configura��o da
 * aplica��o:
 * <ul>
 * <li>versao: Vers�o da aplica��o</li>
 * <li>sdbg: Nome do SGBD utilizado</li>
 * <li>driver: Classe do Driver do SGBD</li>
 * <li>url: Url de conex�o com o SGBD</li>
 * <li>databaseName: Nome do banco de dados</li>
 * <li>user: Nome de usu�rio do SGBD</li>
 * <li>pwd: Senha do usu�rio no SGBD</li>
 * <li>geoPortalKey: Valor da chave de acesso da API GeoPortal</li>
 * <li>delayAPIThread: Tempo em segundos para um ciclo da Thread de leitura de mensagens</li>
 * <li>delayRuleThread: Tempo em segundos para um ciclo da Thread de leitura das regras</li>
 * <li>limitQuerySize: Quantidade m�xima de registros na consulta da tabela de mensagens</li>
 * <li>refreshEntradaTableInterval: Tempo em milissegundos para a atualiza��o da tabela de mensagens de entrada</li>
 * <li>refreshSaidaTableInterval: Tempo em milissegundos para a atualiza��o da tabela de mensagens filtradas</li>
 * </ul>
 * 
 * @author Rafael
 * @author Felipe
 *
 */
public class Configuration {
	
    /**
     * LOGGER
     */
    private static final Logger LOGGER = Logger.getLogger(Configuration.class.getCanonicalName());

    /**
     * Este m�todo retorna uma inst�ncia de {@link Properties} contendo as
     * propriedades de configura��o da aplica��o.
     * 
     * @return Propriedades de configura��o da aplica��o.
     */
    public static Properties getProperties() {
        Properties prop;
        Properties parameters = new Properties();
        try {
            String path = Configuration.class.getProtectionDomain().getCodeSource().getLocation().getPath().substring(1).replace("%20", " ");
            prop = readPropertyFile(path + "parameters/credentials.properties");
            if (prop != null) {
                String fileLocation = prop.getProperty("location");
                parameters = readPropertyFile(fileLocation);
            }
            parameters.setProperty("location", prop.getProperty("location"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return parameters;
    }

    /**
     * 
     * Este m�todo busca um arquivo de propriedades em disco, utilizando o caminho fornecido.
     * 
     * @param filename Caminho absoluto em disco.
     * @return Uma inst�ncia de {@link Properties} lidas do arquivo
     * @throws IOException Indicando erro de leitura.
     */
    private static Properties readPropertyFile(String filename) throws IOException {
    	LOGGER.info("Reading Property File from: " + filename);
        Properties properties = new Properties();
        File propertyFile = new File(filename);
        if (!propertyFile.exists()) {
            LOGGER.warn("File " + filename + " not found in the current directory");
            LOGGER.warn("Trying to load " + filename + " using the classpath");

            InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(filename);
            if (is != null) {
                properties.load(is);
            } else {
                LOGGER.error("File " + filename + " does not exist");
                throw new FileNotFoundException();
            }
        } else {
            FileInputStream fi = new FileInputStream(propertyFile);
            LOGGER.info("Loaded parameters from file: " + propertyFile.getName());
            properties.load(fi);
            fi.close();
        }

        return properties;
    }

    /**
     * Este m�todo retorna o local do arquivo de configura��o.
     * 
     * @return String contendo o caminho em disco do arquivo de configura��o.
     */
    public static String getPathProperties() {
        Properties prop = Configuration.getProperties();
        if (prop != null) {
            return prop.getProperty("location");
        } else {
            return null;
        }
    }
}
