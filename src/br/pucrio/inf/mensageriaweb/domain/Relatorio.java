package br.pucrio.inf.mensageriaweb.domain;

import br.pucrio.inf.mensageriaweb.database.ConnectionSGBD;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Classe para montagem das estat�sticas da quantidade de mensagens por hora.
 * A busca � feita no banco de dados.
 *
 * @author Rafael
 * @author Felipe
 * @author Stephanie
 * @author Pedro
 */
public class Relatorio {

    /**
     * M�todo que monta uma String no formato definido pela API Google Charts, para
     * a montagem do gr�fico de mensagens por hora.
     * 
     * @return String para a montagem do gr�fico de mensagens por hora.
     * 
     * @see <a href="https://developers.google.com/chart/">Google Charts API</a>
     */
    public String getDadosRelatorioMensagensPorHora() {
        String dadosGrafico = "";
        String cabecalhoString = "";
        ConnectionSGBD connection = ConnectionSGBD.getInstance();
        try {
            if (connection != null) {
                PreparedStatement pps = connection.getConnection().prepareStatement("select * from v_relatorio_mensagens",
                        ResultSet.TYPE_SCROLL_SENSITIVE,
                        ResultSet.CONCUR_UPDATABLE);
                ResultSet rs = pps.executeQuery();
                String hora = "";

                ArrayList<String> cabecalho = new ArrayList<>();
                cabecalho.add("Hora");
                while (rs.next()) {
                    if (!cabecalho.contains(rs.getString(2).trim())) {
                        cabecalho.add(rs.getString(2).trim());
                    }
                }
                for (int i = 0; i < cabecalho.size(); i++) {
                    cabecalhoString += "'" + cabecalho.get(i) + "'";
                    if (i + 1 < cabecalho.size()) {
                        cabecalhoString += ", ";
                    }
                }
                cabecalhoString = "[" + cabecalhoString + "],\n";
                rs.beforeFirst();
                while (rs.next()) {
                    if (!hora.equals(formataData(rs.getString(1)))) {
                        hora = formataData(rs.getString(1));
                        if (!dadosGrafico.isEmpty()) {
                            dadosGrafico += "]";
                            if (!rs.isLast()) {
                                dadosGrafico += ",";
                            }
                            dadosGrafico += "\n";
                        }
                        dadosGrafico += "['" + hora + "', ";
                    } else {
                        dadosGrafico += ", ";
                    }
                    dadosGrafico += rs.getString(3);
                    if (rs.isLast()) {
                        dadosGrafico += "]";
                    }
                }
            }
            dadosGrafico = "[" + cabecalhoString + dadosGrafico + "]";
        } catch (SQLException ex) {
            System.err.print(ex);
        }
        return dadosGrafico;
    }

    /**
     * M�todo para formatar datas.
     * 
     * @param data no formato yyyy-MM-dd hh:mm:ss
     * @return String com a data no formato dd/MM - HH'h'
     */
    private String formataData(String data) {
        try {
            SimpleDateFormat dateFormatOriginal = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date date = dateFormatOriginal.parse(data);
            SimpleDateFormat dateFormatFromView = new SimpleDateFormat("dd/MM - HH'h'");
            data = dateFormatFromView.format(date);
        } catch (ParseException ex) {
            System.err.print(ex);
        }
        return data;
    }

}
