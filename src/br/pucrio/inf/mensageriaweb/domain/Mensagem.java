package br.pucrio.inf.mensageriaweb.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Classe de modelo que representa uma mensagem de entrada no sistema.
 * 
 * <p>Uma mensagem � identificada unicamente por um ID de mensagem e um ID de categoria.
 * 
 * <p>Uma mensagem possui os seguintes atributos:
 * <ul>
 * <li>description: Descri��o do conte�do da mensagem</li>
 * <li>citicity: Criticidade da mensagem no sistema</li>
 * <li>timestamp: Data e Hora na qual a mensagem entrou no sistema</li>
 * <li>icon: �cone que representa a mensagem</li>
 * <li>title: T�tulo da mensagem</li>
 * <li>latitude: Latitude geogr�fica da mensagem</li>
 * <li>longitude: Longitude geogr�fica da mensagem</li>
 * <li>category: Descri��o da categoria da mensagem</li>
 * <li>situation: Situa��o da mensagem (aberta ou fechada)</li>
 * </ul>
 * 
 * <p>Essa classe possui uma subclasse {@link Builder} como padr�o de constru��o de uma
 * mensagem.
 * 
 * @author Felipe
 *
 */
public class Mensagem implements Serializable {

    private static final long serialVersionUID = -1969723390307359810L;
	// IDs
    protected int id;
    protected int categoryID;

    // Data
    protected String description;
    protected Timestamp timestamp;
    protected String icon;
    protected String title;
    protected double latitude;
    protected double longitude;
    protected String category;
    protected String situation;

    public Mensagem() {
    }

    public Mensagem(Builder builder) {
        this.id = builder.builderID;
        this.categoryID = builder.builderCategoryID;

        this.description = builder.builderDescription;
        this.timestamp = builder.builderTimestamp;
        this.icon = builder.builderIcon;
        this.title = builder.builderTitle;
        this.latitude = builder.builderLatitude;
        this.longitude = builder.builderLongitude;
        this.category = builder.builderCategory;
        this.situation = builder.builderSituation;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("MSG\n\t ID: ").append(id).append("\n");
        sb.append("\t Category ID: ").append(categoryID).append("\n");
        sb.append("\t Description: ").append(description).append("\n");
        sb.append("\t Timestamp: ").append(timestamp).append("\n");
        sb.append("\t Icon: ").append(icon).append("\n");
        sb.append("\t Title: ").append(title).append("\n");
        sb.append("\t Latitude: ").append(latitude).append("\n");
        sb.append("\t Longitude: ").append(longitude).append("\n");
        sb.append("\t Category: ").append(category).append("\n");
        sb.append("\t Situation: ").append(situation).append("\n");

        return sb.toString();
    }
    
    @Override
    public boolean equals(Object obj) {
    	if (!(obj instanceof Mensagem))
            return false;
        if (obj == this)
            return true;

        Mensagem msg = (Mensagem) obj;
        return new EqualsBuilder().
            append(id, msg.id).
            append(categoryID, msg.categoryID).
            isEquals();
    }
    
    @Override
    public int hashCode() {
    	return new HashCodeBuilder(17, 31).
                append(id).
                append(categoryID).
                toHashCode();
    }

    //////////////////////////////////////////////////////////////////////////
    // Builder
    //////////////////////////////////////////////////////////////////////////
    public static class Builder {
    	//IDs
        protected int builderID;
        protected int builderCategoryID;
    	
    	// Data
    	protected String builderDescription;
        protected Timestamp builderTimestamp;
        protected String builderIcon;
        protected String builderTitle;
        protected double builderLatitude;
        protected double builderLongitude;
        protected String builderCategory;
        protected String builderSituation;

        public Builder() {
        }

        public Builder id(int id) {
            this.builderID = id;
            return this;
        }

        public Builder categoryID(int categoryID) {
            this.builderCategoryID = categoryID;
            return this;
        }

        public Builder description(String description) {
        	this.builderDescription = description.replaceAll("<[^>]*>", " ");
        	return this;
        }
        
        public Builder timestamp(Timestamp timestamp) {
        	this.builderTimestamp = timestamp;
        	return this;
        }
        
        public Builder icon(String icon) {
            this.builderIcon = icon;
            return this;
        }
        
        public Builder title(String title) {
            this.builderTitle = title;
            return this;
        }
        
        public Builder latitude(double latitude) {
        	this.builderLatitude = latitude;
        	return this;
        }
        
        public Builder longitude(double longitude) {
        	this.builderLongitude = longitude;
        	return this;
        }
        
        public Builder category(String category) {
            this.builderCategory = category;
            return this;
        }   

        public Builder situation(String situation) {
            this.builderSituation = situation;
            return this;
        }              

        public Mensagem build() {
            return new Mensagem(this);
        }

    }
    
	//////////////////////////////////////////////////////////////////////////
	// Gets and Sets
	//////////////////////////////////////////////////////////////////////////

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCategoryID() {
		return categoryID;
	}

	public void setCategoryID(int categoryID) {
		this.categoryID = categoryID;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description.replaceAll("<[^>]*>", " ");;
	}

	public Timestamp getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getSituation() {
		return situation;
	}

	public void setSituation(String situation) {
		this.situation = situation;
	}
}