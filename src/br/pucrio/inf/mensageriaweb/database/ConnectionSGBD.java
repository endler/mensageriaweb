package br.pucrio.inf.mensageriaweb.database;

import br.pucrio.inf.mensageriaweb.domain.Configuration;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe Singleton de conex�o com o banco de dados.
 * 
 * @author Felipe
 * @author Rafael
 *
 */
public class ConnectionSGBD {

    /**
     * Conex�o
     */
    protected static Connection connection;
    /**
     * Inst�ncia Singleton da classe de conex�o com o banco
     */
    private static ConnectionSGBD singleton;
    /**
     * Arquivo de propriedades de configura��o da aplica��o
     */
    public Properties config;

    /**
     * M�todo para criar uma conex�o com o banco de dados de acordo com os par�metros
     * lidos do arquivo de configura��o.
     */
    private void connect() {
        if (connection == null) {
            try {
                config = Configuration.getProperties();
                switch (config.getProperty("sgbd")) {
                    case "sqlserver":
                        connection = DriverManager.getConnection(config.getProperty("urlSQLServer") + "databaseName=" + config.getProperty("databaseName") + ";", config.getProperty("userSQLServer"), config.getProperty("pwdSQLServer"));
                        break;
                    case "postgresql":
                        Class.forName("org.postgresql.Driver");
                        connection = DriverManager.getConnection(config.getProperty("urlPostgres") + config.getProperty("databaseName"), config.getProperty("userPostgres"), config.getProperty("pwdPostgres"));
                        break;
                    case "oracle":
                        connection = DriverManager.getConnection(config.getProperty("urlOracle") + config.getProperty("databaseName"), config.getProperty("userOracle"), config.getProperty("pwdOracle"));
                        break;
                    default:
                        throw new UnsupportedOperationException("Atributo SGBD do arquivo de par�metros (.properties) nao foi atribuido corretamente.");
                }
            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(ConnectionSGBD.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    /**
     * Construtor padr�o que inicia uma conex�o.
     */
    private ConnectionSGBD() {
        connect();
    }

    /**
     * @return Uma inst�ncia da classe de conex�o com o banco de dados.
     */
    public static ConnectionSGBD getInstance() {
        if (singleton == null) {
            singleton = new ConnectionSGBD();
        }
        return singleton;
    }

    /**
     * @return A conex�o com o banco de dados.
     */
    public Connection getConnection() {
        return connection;
    }
}