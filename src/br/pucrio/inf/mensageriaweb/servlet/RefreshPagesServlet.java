package br.pucrio.inf.mensageriaweb.servlet;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import br.pucrio.inf.mensageriaweb.domain.Configuration;

/**
 * Esta classe implementa um {@link HttpServlet} que retorna atrav�s do m�todo
 * {@link #doGet(HttpServletRequest, HttpServletResponse)} um <i>HTTP GET</i>
 * contendo os tempos de atualiza��o das tabelas de Entrada e Sa�da do 
 * MensageriaWeb em milissegundos.
 * 
 * @author Felipe
 *
 */
@WebServlet(value = "/refreshPagesServlet", name = "RefreshPagesServlet")
public class RefreshPagesServlet extends HttpServlet {

	private static final long serialVersionUID = -4504924830776498826L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Map<String, String> options = new LinkedHashMap<String, String>();
	    options.put("refreshTimeEntrada", Configuration.getProperties().getProperty("refreshEntradaTableInterval"));
	    options.put("refreshTimeSaida", Configuration.getProperties().getProperty("refreshSaidaTableInterval"));
	    
	    String json = new Gson().toJson(options);

	    response.setContentType("application/json");
	    response.setCharacterEncoding("UTF-8");
	    response.getWriter().write(json);
	}
}
