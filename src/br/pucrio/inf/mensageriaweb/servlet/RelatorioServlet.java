/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pucrio.inf.mensageriaweb.servlet;

import br.pucrio.inf.mensageriaweb.database.ConnectionSGBD;
import br.pucrio.inf.mensageriaweb.domain.MensagemFiltrada;
import br.pucrio.inf.mensageriaweb.domain.Relatorio;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "RelatorioServlet", urlPatterns = {"/RelatorioServlet"})
public class RelatorioServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Relatorio relatorio = new Relatorio();
        String data = relatorio.getDadosRelatorioMensagensPorHora();
            int todasOcorrencias = 0;
        List<String> nomeFiltros = new ArrayList<>();
        List<Integer> qtdFiltros = new ArrayList<>();

        Set<MensagemFiltrada> mSet = new LinkedHashSet<MensagemFiltrada>();
        mSet.clear();
        try {
            ConnectionSGBD connection = ConnectionSGBD.getInstance();
            if (connection != null) {
                PreparedStatement pps = connection.getConnection().prepareStatement("select * from v_mensagens_filtradas");
                ResultSet rs = pps.executeQuery();
                while (rs.next()) {
                    if (nomeFiltros.contains(rs.getString("nome_regra"))) {
                        int index = nomeFiltros.indexOf(rs.getString("nome_regra"));
                        int buffer = qtdFiltros.get(index);
                        buffer += 1;
                        qtdFiltros.set(index, buffer);
                    } else {
                        nomeFiltros.add(rs.getString("nome_regra"));
                        qtdFiltros.add(1);
                    }
                    todasOcorrencias += 1;
                }
                int integrator;
                for (integrator = 0; integrator < nomeFiltros.size(); integrator++) {
                    nomeFiltros.set(integrator, "'" + nomeFiltros.get(integrator) + "'");
                }
                nomeFiltros.add("'Todas Ocorrências Filtradas'");
                qtdFiltros.add(todasOcorrencias);
            }
        } catch (SQLException ex) {
            Logger.getLogger(MapaServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

        request.setAttribute("filtro", nomeFiltros);
        request.setAttribute("quantidade", qtdFiltros);

        request.setAttribute("dadosRelatorioMensagensPorHora", data);
        request.getRequestDispatcher("/relatorio.jsp").forward(request, response);
    }
}
