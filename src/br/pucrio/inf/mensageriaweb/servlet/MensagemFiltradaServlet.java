package br.pucrio.inf.mensageriaweb.servlet;

import br.pucrio.inf.mensageriaweb.database.ConnectionSGBD;
import br.pucrio.inf.mensageriaweb.domain.MensagemFiltrada;
import br.pucrio.inf.mensageriaweb.domain.Regra;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Esta classe implementa um {@link HttpServlet} que retorna atrav�s do m�todo
 * {@link #doPost(HttpServletRequest, HttpServletResponse)} um <i>HTTP POST</i>
 * contendo o conte�do das mensagens filtradas do banco de dados.
 *
 * @author Felipe
 *
 */
@WebServlet(value = "/mensagemFiltradaServlet", name = "MsgFiltradaServlet")
public class MensagemFiltradaServlet extends HttpServlet {

    private static final long serialVersionUID = -4504924830776498826L;

    /**
     * {@link Set} de mensagens filtradas.
     */
    private Set<MensagemFiltrada> filteredMessagesSet;

    /**
     *
     */
    public MensagemFiltradaServlet() {
        filteredMessagesSet = new LinkedHashSet<MensagemFiltrada>();
    }

    /**
     * Este m�todo retorna um JSON em formato {@link String} constru�do a partir
     * de um {@link Set} de mensagens filtradas armazenadas em um
     * {@link LinkedHashSet}.
     * <p>
     * A constru��o � feita utilizando a biblioteca {@link Gson}.
     *
     * @return JSON com o conte�do do {@link Set}.
     */
    private String getSetJSON() {
        Gson gson = new GsonBuilder()
                .setDateFormat("dd/MM/yyyy HH:mm:ss")
                .create();
        String json = gson.toJson(filteredMessagesSet);
        return json;
    }

    /**
     * Este m�todo l� todas as mensagens filtradas armazenadas no banco de dados
     * e as armazena em um {@link LinkedHashSet}.
     */
    private void readAllFilteredMessages() {
        try {
            filteredMessagesSet.clear();
            ConnectionSGBD connection = ConnectionSGBD.getInstance();
            if (connection != null) {
                PreparedStatement pps = connection.getConnection().prepareStatement("select * from v_mensagens_filtradas");
                ResultSet rs = pps.executeQuery();
                while (rs.next()) {
                    Regra regra = new Regra(rs.getInt("id_regra"), rs.getString("nome_regra"), rs.getString("regra"));
                    MensagemFiltrada.Builder msgBuilder = new MensagemFiltrada.Builder();
                    String poi = rs.getString("poi");
                    if (poi == null) {
                        poi = "";
                    }
                    msgBuilder.id(rs.getInt("id_mensagem"))
                            .categoryID(rs.getInt("id_categoria"))
                            .description(rs.getString("descricao"))
                            .criticity(rs.getString("criticidade"))
                            .timestamp(rs.getTimestamp("data"))
                            .icon(rs.getString("icone"))
                            .title(rs.getString("titulo"))
                            .latitude(rs.getDouble("geo_latitude"))
                            .longitude(rs.getDouble("geo_longitude"))
                            .category(rs.getInt("id_categoria") + " - " + rs.getString("categoria"))
                            .situation(rs.getString("situacao"))
                            .poi(poi)
                            .filtro(regra);
                    MensagemFiltrada msg = msgBuilder.build();
                    filteredMessagesSet.add(msg);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(MensagemFiltradaServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        readAllFilteredMessages();
        String json = getSetJSON();
        out.print(json);
    }
}
