package br.pucrio.inf.mensageriaweb.servlet;

import br.pucrio.inf.mensageriaweb.database.ConnectionSGBD;
import br.pucrio.inf.mensageriaweb.domain.Configuration;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = "/MapaServlet", name = "MapaServlet")
public class MapaServlet extends HttpServlet {

    private List<String> lst;
    private List<String> heatmap;
    private List<String> attributesDesc;
    private List<Integer> categorias;
    private List<String> poi = null;
    private List<List<String>> allPoi;
    private int limitQuerySize;
    private static final long serialVersionUID = 1L;

    public MapaServlet() {
        Properties config = Configuration.getProperties();
        this.limitQuerySize = Integer.parseInt(config.getProperty("limitQuerySize"));
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        lst = new ArrayList<>();

        attributesDesc = new ArrayList<>();
        categorias = new ArrayList<>();
        try {
            ConnectionSGBD connection = ConnectionSGBD.getInstance();
            if (connection != null) {
                PreparedStatement pps = connection.getConnection().prepareStatement("select * from v_mensagens_filtradas");
                ResultSet rs = pps.executeQuery();
                while (rs.next()) {
                    cleanString(rs.getString("descricao"), rs.getString("categoria"), rs.getString("nome_regra"), rs.getString("id_mensagem"));
                    verificaCategoria(rs.getString("categoria"));
                    lst.add("{ lat: " + rs.getDouble("geo_latitude") + ", lng: " + rs.getDouble("geo_longitude") + " }");
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(MapaServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        allPoi = new ArrayList<>();
        String ultimoPoi = "";
        try {
            ConnectionSGBD connection = ConnectionSGBD.getInstance();
            if (connection != null) {
                PreparedStatement pps = connection.getConnection().prepareStatement("select * from v_poi");
                ResultSet rs = pps.executeQuery();

                while (rs.next()) {
                    if (poi == null || !(ultimoPoi.contains(rs.getString("regiao")))) {
                        if (poi != null) {
                            allPoi.add(poi);
                        }
                        poi = new ArrayList<>();
                        ultimoPoi = rs.getString("regiao");
                    }
                    poi.add("{ lat: " + rs.getDouble("latitude") + ", lng: " + rs.getDouble("longitude") + " }");
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(MapaServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

        heatmap = new ArrayList<>();
        try {
            ConnectionSGBD connection = ConnectionSGBD.getInstance();
            if (connection != null) {
                PreparedStatement pps = connection.getConnection().prepareStatement("select geo_latitude, geo_longitude from mensagens where timestamp >= NOW() - '1 day'::INTERVAL order by timestamp desc LIMIT " + limitQuerySize);
                ResultSet rs = pps.executeQuery();

                while (rs.next()) {
                    heatmap.add("'" + rs.getDouble("geo_latitude") + "'");
                    heatmap.add("'" + rs.getDouble("geo_longitude") + "'");

                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(MapaServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        request.setAttribute("poi", allPoi);
        request.setAttribute("verificador", categorias);
        request.setAttribute("attributesDesc", attributesDesc);
        request.setAttribute("lst", lst);
        request.setAttribute("heatmap", heatmap);
        request.getRequestDispatcher("/mapa.jsp").forward(request, response);

    }

    protected void cleanString(String desc, String cat, String filtro, String id) {
        attributesDesc.add("'<strong>ID: </strong>" + id
                + " / <strong>Filtro: </strong>" + filtro
                + "<br><br><strong>Descri��o: </strong>" + desc.replace("\n", "<br>")
                .replace("]", ")")
                .replace("[", "(")
                .trim()
                + "<br><strong>Categoria: </strong>" + cat + "'");
    }

    protected void verificaCategoria(String cat) {
        if (cat.contains("Waze")) {
            categorias.add(1);
        } else {
            categorias.add(0);
        }
    }
}
