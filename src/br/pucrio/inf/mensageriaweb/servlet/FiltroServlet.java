package br.pucrio.inf.mensageriaweb.servlet;

import br.pucrio.inf.mensageriaweb.database.ConnectionSGBD;
import br.pucrio.inf.mensageriaweb.domain.Regra;
import br.pucrio.inf.mensageriaweb.domain.SituacaoRegra;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Esta classe implementa um {@link HttpServlet} que retorna atrav�s do m�todo
 * {@link #doPost(HttpServletRequest, HttpServletResponse)} um <i>HTTP POST</i>
 * contendo os filtros do banco de dados.
 * 
 * @author Felipe
 *
 */
@WebServlet(value = "/filtroServlet", name = "FiltroServlet")
public class FiltroServlet extends HttpServlet {

    private static final long serialVersionUID = -4504924830776498826L;

    /**
     * {@link Set} de filtros.
     */
    private Set<Regra> filterSet;

    public FiltroServlet() {
        filterSet = new LinkedHashSet<Regra>();
    }

    /**
     * Este m�todo retorna um JSON em formato {@link String} constru�do a partir
     * de um {@link Set} de filtros armazenados em um {@link LinkedHashSet}.
     * <p>
     * A constru��o � feita utilizando a biblioteca {@link Gson}.
     * 
     * @return JSON com o conte�do do {@link Set}. 
     */
    public String getSetJSON() {
        Gson gson = new GsonBuilder()
                .setDateFormat("dd/MM/yyyy HH:mm:ss")
                .create();
        String json = gson.toJson(filterSet);
        return json;
    }
    
    /**
     * Este m�todo l� todos os filtros armazenados no banco de dados
     * e os armazena em um {@link LinkedHashSet}.
     */
    private void readAllFilters() {
        try {
            filterSet.clear();
            ConnectionSGBD connection = ConnectionSGBD.getInstance();
            if (connection != null) {
                PreparedStatement pps = connection.getConnection().prepareStatement("select * from regras order by id_regra");
                ResultSet rs = pps.executeQuery();
                while (rs.next()) {
                    filterSet.add(new Regra(rs.getInt(1), rs.getString(2), rs.getString(3), SituacaoRegra.valueOf(rs.getString(4))));
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(FiltroServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        readAllFilters();
        String json = getSetJSON();
        out.print(json);
    }
}
