package br.pucrio.inf.mensageriaweb.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.pucrio.inf.mensageriaweb.domain.Regra;

/**
 * Esta classe implementa um {@link HttpServlet} que recebe atrav�s do m�todo
 * {@link #doPost(HttpServletRequest, HttpServletResponse)} um ID de filtro para
 * exclus�o no banco de dados.
 * 
 * @author Felipe
 *
 */
@WebServlet(value = "/filtroDeleteServlet", name = "FiltroDeleteServlet")
public class FiltroDeleteServlet extends HttpServlet {

    private static final long serialVersionUID = -4504924830776498826L;

    public FiltroDeleteServlet() {
    	super();
    }    

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("UTF-8");
        String submitButton = request.getParameter("submitButton");
        int id = Integer.parseInt(request.getParameter("hiddenIdDeleteFilter"));
        Regra regra = new Regra(id);
        if (submitButton.equals("excluir")) {
        	regra.delete();
        }
        response.setHeader("Refresh", "0; URL=/mensageriaweb/#filtros");
    }
}
