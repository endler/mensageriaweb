package br.pucrio.inf.mensageriaweb.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.pucrio.inf.mensageriaweb.domain.Regra;

/**
 * Esta classe implementa um {@link HttpServlet} que recebe atrav�s do m�todo
 * {@link #doPost(HttpServletRequest, HttpServletResponse)} o ID de um filtro, 
 * uma regra e o nome desta regra para altera��o dos dados no banco de dados.
 * 
 * @author Felipe
 *
 */
@WebServlet(value = "/filtroEditorServlet", name = "FiltroEditorServlet")
public class FiltroEditorServlet extends HttpServlet {

    private static final long serialVersionUID = -4504924830776498826L;

    public FiltroEditorServlet() {
    	super();
    }    

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("UTF-8");
        String submitButton = request.getParameter("submitButton");
        int id = Integer.parseInt(request.getParameter("filterID"));
        String nome = request.getParameter("filterName");
        String epl = request.getParameter("filterEPL").replace("\n", " ");
        Regra filtro = new Regra(id, nome, epl);
        if (submitButton.equals("editar")) {
        	filtro.save();
        }
        response.setHeader("Refresh", "0; URL=/mensageriaweb/#filtros");
    }
}
