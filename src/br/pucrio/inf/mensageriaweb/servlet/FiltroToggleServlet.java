package br.pucrio.inf.mensageriaweb.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.pucrio.inf.mensageriaweb.domain.Regra;
import br.pucrio.inf.mensageriaweb.domain.SituacaoRegra;

/**
 * Esta classe implementa um {@link HttpServlet} que recebe atrav�s do m�todo
 * {@link #doPost(HttpServletRequest, HttpServletResponse)} um ID de filtro e sua situa��o
 * para ativa��o ou desativa��o no banco de dados.
 * 
 * @author Felipe
 *
 */
@WebServlet(value = "/filtroToggleServlet", name = "FiltroToggleServlet")
public class FiltroToggleServlet extends HttpServlet {

    private static final long serialVersionUID = -4504924830776498826L;

    public FiltroToggleServlet() {
    	super();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("UTF-8");
        int id = Integer.parseInt(request.getParameter("hiddenIdFilter"));
        String status = request.getParameter("hiddenStatusFilter");
        Regra filtro = new Regra(id, SituacaoRegra.valueOf(status));
        filtro.changeStatus();
        response.setHeader("Refresh", "0; URL=/mensageriaweb/#filtros");
    }
}
