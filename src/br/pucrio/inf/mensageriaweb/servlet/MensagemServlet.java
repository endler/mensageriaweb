package br.pucrio.inf.mensageriaweb.servlet;

import br.pucrio.inf.mensageriaweb.database.ConnectionSGBD;
import br.pucrio.inf.mensageriaweb.domain.Configuration;
import br.pucrio.inf.mensageriaweb.domain.Mensagem;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashSet;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Esta classe implementa um {@link HttpServlet} que retorna atrav�s do m�todo
 * {@link #doPost(HttpServletRequest, HttpServletResponse)} um <i>HTTP POST</i>
 * contendo o conte�do das mensagens do banco de dados.
 * 
 * @author Felipe
 *
 */
@WebServlet(value = "/mensagemServlet", name = "MsgServlet")
public class MensagemServlet extends HttpServlet {

    private static final long serialVersionUID = -4504924830776498826L;

    /**
     * {@link Set} de mensagens.
     */
    private Set<Mensagem> messagesSet;
    
    /**
     * Limite para a quantidade de mensagens vindas do banco de dados.
     */
    private int limitQuerySize;

    /**
     * Construtor <i>default</i> que inicia o {@link Set} de mensagens 
     * e seta o valor do limite de mensagens.
     * 
     */
    public MensagemServlet() {
        messagesSet = new LinkedHashSet<>();
        Properties config = Configuration.getProperties();
        this.limitQuerySize = Integer.parseInt(config.getProperty("limitQuerySize", "5000"));
    }
    
    /**
     * Este m�todo retorna um JSON em formato {@link String} constru�do a partir
     * de um {@link Set} de mensagens filtradas armazenadas em um {@link LinkedHashSet}.
     * <p>
     * A constru��o � feita utilizando a biblioteca {@link Gson}.
     * 
     * @return JSON com o conte�do do {@link Set}. 
     */
    private String getSetJSON() {
        Gson gson = new GsonBuilder()
                .setDateFormat("dd/MM/yyyy HH:mm:ss")
                .create();
        String json = gson.toJson(messagesSet);
        return json;
    }
    
    /**
     * Este m�todo l� todas as mensagens filtradas armazenadas no banco de dados
     * e as armazena em um {@link LinkedHashSet}.
     */
    private void readAllNewMessages() {
        try {
            messagesSet.clear();
            ConnectionSGBD connection = ConnectionSGBD.getInstance();
            if (connection != null) {
                PreparedStatement pps = connection.getConnection().prepareStatement("select * from v_mensagens_entrada LIMIT " + limitQuerySize);
                ResultSet rs = pps.executeQuery();
                while (rs.next()) {
                    Mensagem.Builder msgBuilder = new Mensagem.Builder();
                    msgBuilder.id(rs.getInt("id_mensagem"))
                            .categoryID(rs.getInt("id_categoria"))
                            .description(rs.getString("descricao"))
                            .timestamp(rs.getTimestamp("data"))
                            .icon(rs.getString("icone"))
                            .title(rs.getString("titulo"))
                            .latitude(rs.getDouble("geo_latitude"))
                            .longitude(rs.getDouble("geo_longitude"))
                            .category(rs.getInt("id_categoria") + " - " + rs.getString("categoria"))
                            .situation(rs.getString("situacao"));
                    Mensagem msg = msgBuilder.build();
                    messagesSet.add(msg);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(MensagemServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {        
        Properties config = Configuration.getProperties();
        this.limitQuerySize = Integer.parseInt(config.getProperty("limitQuerySize", "5000"));
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        readAllNewMessages();
        String json = getSetJSON();
        out.print(json);
    }
}
