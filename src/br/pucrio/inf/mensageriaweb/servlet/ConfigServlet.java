package br.pucrio.inf.mensageriaweb.servlet;

import br.pucrio.inf.mensageriaweb.domain.Configuration;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Esta classe implementa um {@link HttpServlet} que retorna atrav�s do m�todo
 * {@link #doGet(HttpServletRequest, HttpServletResponse)} um <i>HTTP GET</i>
 * contendo os par�metros de configura��o das aplica��es Mensageria e MensageriaWeb
 * 
 * <p> Atrav�s do m�todo {@link #doPost(HttpServletRequest, HttpServletResponse)} 
 * este Servlet recebe os par�metros que foram alterados na tela de configura��o
 * da aplica��o MensageriaWeb.
 * 
 * @author Stephanie
 * @author Pedro
 * @author Felipe
 * @author Rafel
 *
 */
@WebServlet(urlPatterns = "/ConfigServlet", name = "ConfigServlet")
public class ConfigServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    public ConfigServlet() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Properties atributos = Configuration.getProperties();
        atributos.remove("location");
        request.setAttribute("atributos", atributos);
        request.getRequestDispatcher("/config.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setCharacterEncoding("UTF-8");
        List<String> parameterNames = new ArrayList<>(request.getParameterMap().keySet());
        try {
            File f1 = new File(Configuration.getPathProperties());
            FileWriter fw = new FileWriter(f1);
            BufferedWriter out = new BufferedWriter(fw);
            for (String parameterName : parameterNames) {
                out.write(parameterName + "=" + request.getParameter(parameterName));
                out.newLine();
            }
            out.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        response.setHeader("Refresh", "0; URL=/mensageriaweb");
    }
}
