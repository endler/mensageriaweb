package br.pucrio.inf.mensageriaweb.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.pucrio.inf.mensageriaweb.domain.Regra;

/**
 * Esta classe implementa um {@link HttpServlet} que recebe atrav�s do m�todo
 * {@link #doPost(HttpServletRequest, HttpServletResponse)} um novo Filtro para
 * inclus�o no banco de dados.
 * 
 * @author Felipe
 *
 */
@WebServlet(value = "/filtroNovoServlet", name = "FiltroNovoServlet")
public class FiltroNovoServlet extends HttpServlet {

    private static final long serialVersionUID = -4504924830776498826L;

    public FiltroNovoServlet() {
    	super();
    }    

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("UTF-8");
        String nome = request.getParameter("newFilterName");
        String regra = request.getParameter("newFilterRegra").replace("\n", " ");
        Regra filtro = new Regra(nome, regra);
        filtro.save();
        response.setHeader("Refresh", "0; URL=/mensageriaweb/#filtros");
    }
}
