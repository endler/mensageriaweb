package br.pucrio.inf.mensageriaweb.database;

import static org.junit.Assert.assertTrue;
import org.junit.Test;

import br.pucrio.inf.mensageriaweb.database.ConnectionSGBD;

/**
 *
 * @author Felipe
 */
public class ConnectionSGBDTest {
	
	/**
	 * Verificando se o banco de dados est� online.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testaConexaoSGBD() throws Exception {		
		assertTrue("Conex�o com o Banco n�o foi poss�vel", ConnectionSGBD.getInstance().getConnection() != null);
	}
}
