package br.pucrio.inf.mensageriaweb.servlet;

import static org.junit.Assert.assertFalse;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import br.pucrio.inf.mensageriaweb.database.ConnectionSGBD;
import br.pucrio.inf.mensageriaweb.domain.Regra;

/**
 * @author Felipe
 *
 */
public class FiltroDeleteServletTest extends Mockito {

	/**
	 * Inserindo um registro no banco de dados para ser deletado pelo servlet
	 */
	@Before
	public void insert() {
		try {
			ConnectionSGBD conn = ConnectionSGBD.getInstance();
			PreparedStatement pps = conn.getConnection().prepareStatement("INSERT INTO regras(id_regra, regra, nome, situacao) "
					+ "VALUES (30, 'teste', 'teste', 'ativa')");
			pps.execute();
		} catch (SQLException ex) {
			Logger.getLogger(Regra.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * Deletando o registro e testando se ele foi exclu�do do banco de dados.
	 * 
	 * @throws Exception
	 * @throws SQLException
	 */
	@Test
	public void testDeleteServlet() throws Exception, SQLException {
		HttpServletRequest request = mock(HttpServletRequest.class);       
		HttpServletResponse response = mock(HttpServletResponse.class);

		when(request.getParameter("submitButton")).thenReturn("excluir");
		when(request.getParameter("hiddenIdDeleteFilter")).thenReturn("30");

		FiltroDeleteServlet filtro = new FiltroDeleteServlet();
		filtro.doPost(request, response);
		verify(response, atLeast(1)).setHeader("Refresh", "0; URL=/mensageriaweb/#filtros");

		ConnectionSGBD connection = ConnectionSGBD.getInstance();
		if (connection != null) {
			PreparedStatement pps = connection.getConnection().prepareStatement("select * from regras where id_regra=30");
			ResultSet rs = pps.executeQuery();
			assertFalse("Regra continua no banco de dados",rs.next());
		}
	}
}
