package br.pucrio.inf.mensageriaweb.servlet;

import static org.junit.Assert.assertEquals;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import br.pucrio.inf.mensageriaweb.database.ConnectionSGBD;
import br.pucrio.inf.mensageriaweb.domain.Regra;

/**
 * @author Felipe
 *
 */
public class FiltroToggleServletTest extends Mockito {
	
	/**
	 * Inserindo um registro no banco de dados para ser desativado pelo servlet
	 */
	@Before
	public void insert() {
		try {
			ConnectionSGBD conn = ConnectionSGBD.getInstance();
			PreparedStatement pps = conn.getConnection().prepareStatement("INSERT INTO regras(id_regra, regra, nome, situacao) "
					+ "VALUES (30, 'teste', 'teste', 'ativa')");
			pps.execute();
		} catch (SQLException ex) {
			Logger.getLogger(Regra.class.getName()).log(Level.SEVERE, null, ex);
		}
	}	

	/**
	 * Alterando o estado do filtro e verificando no banco de dados.
	 * 
	 * @throws Exception
	 * @throws SQLException
	 */
	@Test
	public void testeToggleServlet() throws Exception, SQLException {
		HttpServletRequest request = mock(HttpServletRequest.class);       
		HttpServletResponse response = mock(HttpServletResponse.class);

		when(request.getParameter("hiddenIdFilter")).thenReturn("30");
		when(request.getParameter("hiddenStatusFilter")).thenReturn("ativa");

		FiltroToggleServlet filtroToggle = new FiltroToggleServlet();
		filtroToggle.doPost(request, response);
		verify(response, atLeast(1)).setHeader("Refresh", "0; URL=/mensageriaweb/#filtros");

		ConnectionSGBD connection = ConnectionSGBD.getInstance();
		if (connection != null) {
			PreparedStatement pps = connection.getConnection().prepareStatement("select * from regras where nome like 'teste' and regra like 'teste'");
			ResultSet rs = pps.executeQuery();
			rs.next();
			assertEquals("inativa", rs.getString("situacao"));
		}
	}
	
	/**
	 * Excluindo registro do banco de dados.
	 * 
	 * @throws SQLException
	 */
	@After
	public void delete() throws SQLException {
            ConnectionSGBD conn = ConnectionSGBD.getInstance();
            PreparedStatement pps = conn.getConnection().prepareStatement("delete from regras where nome like 'teste' and regra like 'teste'");
            pps.execute();
	}
}
