package br.pucrio.inf.mensageriaweb.servlet;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * @author Felipe
 *
 */
public class MensagemServletTest extends Mockito {

	public static WebDriver driver;

	/**
	 * Iniciando o driver Firefox para o Selenium
	 */
	@BeforeClass
	public static void setup () {
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("http://localhost:8080/mensageriaweb");
	}

	/**
	 * Fechando o driver Firefox e o Browser
	 */
	@AfterClass
	public static void closeBrowser(){
		driver.quit();
	}

	/**
	 * Verificando se o servlet retorna um JSON de mensagens.
	 * 
	 * @throws Exception
	 */
	@Test
	public void verificaMensagem() throws Exception {
		HttpServletRequest request = mock(HttpServletRequest.class);       
		HttpServletResponse response = mock(HttpServletResponse.class);

		StringWriter s = new StringWriter();
		PrintWriter out = new PrintWriter(s);
		when(response.getWriter()).thenReturn(out);

		MensagemServlet mensagem = new MensagemServlet();
		mensagem.doPost(request, response);

		verify(response, atLeast(1)).getWriter();
		out.flush();
		s.flush();
		assertFalse("Tabela de mensagens vazia no banco de dados", s.toString().isEmpty());
	}

	/**
	 * Verifica se ao digitar a palavra "copacabana" no campo de pesquisa 
	 * da tabela de mensagens de entrada, o conte�do da tabela cont�m registros
	 * com a palavra "copacabana".
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void verificaElementosProcurados() throws InterruptedException {
		driver.findElement(By.xpath("//input[@aria-controls='entradaTable']")).sendKeys("copacabana");
		WebElement table_element = driver.findElement(By.id("entradaTable"));
        List<WebElement> tr_collection=table_element.findElements(By.xpath("id('entradaTable')/tbody/tr"));
        int col_num;
        boolean teste;
        for(WebElement trElement : tr_collection)
        {
            List<WebElement> td_collection=trElement.findElements(By.xpath("td"));
            col_num=1;
            teste = false;
            for (WebElement tdElement : td_collection) {
            	if (col_num == 1) {
            		tdElement.click();
            		WebElement table_element_detail = driver.findElement(By.id("detail"));
            		List<WebElement> tr_collection_detail = table_element_detail.findElements(By.xpath("id('detail')/tbody/tr"));
            		for (WebElement trElementDetail: tr_collection_detail) {
            			List<WebElement> td_collection_detail=trElementDetail.findElements(By.xpath("td"));
            			for (WebElement tdElementDetail : td_collection_detail) {
            				if (tdElementDetail.getText().toUpperCase().contains("COPACABANA")) {
                            	teste = true;
                            }
						}
            		}
            		tdElement.click();
            	}
                if (tdElement.getText().toUpperCase().contains("COPACABANA")) {
                	teste = true;
                }
                col_num++;
            }
            assertTrue(teste);
        }
	}
}
