package br.pucrio.inf.mensageriaweb.servlet;

import static org.junit.Assert.assertEquals;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.After;
import org.junit.Test;
import org.mockito.Mockito;

import br.pucrio.inf.mensageriaweb.database.ConnectionSGBD;

/**
 * @author Felipe
 *
 */
public class FiltroNovoServletTest extends Mockito {	

	/**
	 * Inserindo o filtro e verificando no banco de dados.
	 * 
	 * @throws Exception
	 * @throws SQLException
	 */
	@Test
	public void testNovoServlet() throws Exception, SQLException {
		HttpServletRequest request = mock(HttpServletRequest.class);       
		HttpServletResponse response = mock(HttpServletResponse.class);

		when(request.getParameter("newFilterName")).thenReturn("teste");
		when(request.getParameter("newFilterRegra")).thenReturn("teste");

		FiltroNovoServlet filtroNovo = new FiltroNovoServlet();
		filtroNovo.doPost(request, response);
		verify(response, atLeast(1)).setHeader("Refresh", "0; URL=/mensageriaweb/#filtros");

		ConnectionSGBD connection = ConnectionSGBD.getInstance();
		if (connection != null) {
			PreparedStatement pps = connection.getConnection().prepareStatement("select * from regras where nome like 'teste' and regra like 'teste'");
			ResultSet rs = pps.executeQuery();
			rs.next();
			assertEquals("teste", rs.getString("regra"));
			assertEquals("teste", rs.getString("nome"));
			assertEquals("ativa", rs.getString("situacao"));
		}
	}
	
	/**
	 * Excluindo registro do banco de dados.
	 * 
	 * @throws SQLException
	 */
	@After
	public void delete() throws SQLException {
            ConnectionSGBD conn = ConnectionSGBD.getInstance();
            PreparedStatement pps = conn.getConnection().prepareStatement("delete from regras where nome like 'teste' and regra like 'teste'");
            pps.execute();
	}
}
