package br.pucrio.inf.mensageriaweb.servlet;

import static org.junit.Assert.assertFalse;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.mockito.Mockito;

import br.pucrio.inf.mensageriaweb.servlet.FiltroServlet;

/**
 * @author Felipe
 *
 */
public class FiltroServletTest extends Mockito {  

	/**
	 * Verificando se o servlet retorna um JSON de filtros.
	 * 
	 * @throws Exception
	 */
	@Test
    public void verificaFiltros() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);       
        HttpServletResponse response = mock(HttpServletResponse.class);

        StringWriter s = new StringWriter();
        PrintWriter out = new PrintWriter(s);
        when(response.getWriter()).thenReturn(out);

        FiltroServlet filtro = new FiltroServlet();
        filtro.doPost(request, response);
        
        verify(response, atLeast(1)).getWriter();
        out.flush();
        s.flush();
        assertFalse("Tabela de filtros vazia no banco de dados", s.toString().isEmpty());
    }
}
