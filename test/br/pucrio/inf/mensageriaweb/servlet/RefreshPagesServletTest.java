package br.pucrio.inf.mensageriaweb.servlet;

import static org.junit.Assert.assertFalse;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.mockito.Mockito;

/**
 * @author Felipe
 *
 */
public class RefreshPagesServletTest extends Mockito {  

	/**
	 * Verificando se o servlet retorna um JSON de valores de atualização
	 * das tabelas vindas do arquivo de configuração.
	 * 
	 * @throws Exception
	 */
	@Test
    public void verificaRefresh() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);       
        HttpServletResponse response = mock(HttpServletResponse.class);

        StringWriter s = new StringWriter();
        PrintWriter out = new PrintWriter(s);
        when(response.getWriter()).thenReturn(out);

        RefreshPagesServlet refresh = new RefreshPagesServlet();
        refresh.doGet(request, response);
        
        verify(response, atLeast(1)).getWriter();
        out.flush();
        s.flush();
        assertFalse("Sem propriedades de atualização no arquivo de configuração", s.toString().isEmpty());
    }
}
