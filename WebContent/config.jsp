<%-- 
    Document    : config.jsp
    Description : Página de configuração do MensageriaWeb
    Created on  : 08/07/2016, 16:05:32
    Author      : Felipe, Rafael, Pedro, Stephanie
--%>
<%@page import="java.util.Set"%>
<%@page import="java.util.Properties"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
    <head>
        <title>MensageriaWeb 1.0</title>
        <link href="css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/theme.css" rel="stylesheet">
        <link href="css/bootstrap-select.css" rel="stylesheet">
        <link href="css/responsive.bootstrap.css" rel="stylesheet">
        <link href="css/bootstrap-switch.min.css" rel="stylesheet">
        <script src="js/bootstrap.js"></script>
        <script src="js/bootstrap-select.js"></script>
        <script src="js/responsive.bootstrap.js"></script>
        <script src="js/bootstrap-switch.min.js"></script>
        <link href='https://fonts.googleapis.com/css?family=Teko:400,300' rel='stylesheet' type='text/css'>

    </head>
    <body>
        <jsp:include page="navbar2.jsp"></jsp:include>

            <div class="container">
                <h1 class="centraliza_texto">Configurações</h1>
                <br>
                <form id="configuracoes" action="ConfigServlet" method="post">
                <%
                    Properties atributos = (Properties) request.getAttribute("atributos");
                    Set<Object> keys = atributos.keySet();
                    for (Object key : keys) {%>
                <label for="<%=key%>"> <%=key%>: </label><input type='text' class="form-control" name="<%=key%>" value="<%=atributos.getProperty((String) key)%>"/><br>
                <% }%>
                <button type="submit" class="btn btn-success"> SALVAR </button>
            </form>
        </div>
        <jsp:include page="footer.jsp"></jsp:include>
    </body>
</html>

