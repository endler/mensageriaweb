<%-- 
    Document    : entrada.jsp
    Description : Página da Tabela de Entrada de mensagens do MensageriaWeb
    Created on  : 15/06/2016, 14:01:43
    Author      : Felipe
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="css/bootstrap.css" rel="stylesheet">
<div>
    <h1>Caixa de Entrada</h1>
    <br>
    <p>

    <div class="form-group" id="categoriaDiv">		
        Categoria: <label id="selectCategoria"></label>
    </div> 

</p>
<br>

<table id="entradaTable" class="table table-hover table-bordered"
       width="100%" border="0" cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th></th>
            <th>ID</th>
            <th>Categoria</th>
            <th>Título</th>
            <th>Situação</th>
            <th>Data / Hora</th>
        </tr>
    </thead>		
    <tbody>

    </tbody>
    <tfoot>
    </tfoot>
</table>
</div>