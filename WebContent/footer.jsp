<%-- 
    Document    : footer.jsp
    Description : Rodap� do MensageriaWeb
    Created on  : 19/07/2016, 11:15:28
    Author      : Felipe, Rafael, Stephanie, Pedro
--%>
<footer id="rodape">
    <p>
        Pontif�cia Universidade Cat�lica do Rio de Janeiro<br>
        PUC-RIO � 2016<br>
        Todos os direitos reservados.<br><br>

        Melhor visualiza��o: Firefox 47.x e Chrome 51.x
    </p>
</footer>