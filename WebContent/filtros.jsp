<%-- 
    Document    : filtros.jsp
    Description : Página da tabela de filtros do MensageriaWeb
    Created on  : 15/06/2016, 14:01:43
    Author      : Felipe
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="css/bootstrap.css" rel="stylesheet">
<div>

    <h1>Filtros</h1>

    <table id="filtrosTable"
           class="table table-hover table-bordered sortable" width="100%"
           border="0" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nome</th>
                <th>EPL</th>
                <th>Situação</th>
                <th>Editar</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>

    <p>
        <button type="button" class="btn btn-success" data-toggle="modal"
                data-target="#newFilterSimple">Novo Filtro</button>
    </p>

    <!-- Modal Novo Filtro -->
    <div class="modal fade" id="newFilter" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Novo Filtro</h4>
                </div>
                <form role="form" class="form-horizontal">
                    <div class="modal-body">
                        <div class="well">
                            <label>Filtro</label>
                            <hr style="height:1px;border:none;color:#333;background-color:#333;margin-top:3px;"/>
                            <div class="form-group">
                                <label for="filter" class="col-sm-3 control-label">Nome</label>
                                <div class="col-sm-9">						
                                    <input type="text" name="filterName" class="form-control" placeholder="Nome do Filtro">
                                </div>
                            </div>					
                            <div class="form-group">
                                <label for="filter" class="col-sm-3 control-label">Fluxo de Entrada</label>
                                <div class="col-sm-9">						
                                    <select class="selectpicker">
                                        <option>Nada</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">							
                                <label id="labelSaida" for="filter" class="col-sm-3 control-label">Fluxo de Saída</label>
                                <div class="entry col-sm-9">
                                    <div id="btnFields" class="input-group">
                                        <input class="form-control" name="fields[]" type="text"
                                               placeholder="Type something" /> 
                                        <span class="input-group-btn">
                                            <button class="btn btn-success btn-add" type="button">
                                                <span class="glyphicon glyphicon-plus"></span>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </div>						
                        </div>
                        <div class="well">
                            <label>Predicados</label>
                            <hr style="height:1px;border:none;color:#333;background-color:#333;margin-top:3px;"/>
                            <div class="form-group">							
                                <label for="filter" class="col-sm-3 control-label">Conteúdo</label>
                                <div class="col-sm-9">						
                                    <select class="selectpicker">
                                        <option>Nada</option>
                                    </select>
                                    <select class="selectpicker">
                                        <option>Nada</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="well">
                            <label>Correlação</label>
                            <hr style="height:1px;border:none;color:#333;background-color:#333;margin-top:3px;"/>
                            <div class="form-group">							
                                <label for="filter" class="col-sm-3 control-label">Conteúdo</label>
                                <div class="col-sm-9">						
                                    <select class="selectpicker">
                                        <option>Nada</option>
                                    </select>
                                    <select class="selectpicker">
                                        <option>Nada</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Salvar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <!-- Modal Editing Filter -->
    <div class="modal fade" id="editFilter" tabindex="-1" role="dialog" aria-labelledby="ModalEditFilter">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="ModalEditFilter">Editar Filtro</h4>
                </div>
                <form role="form" class="form-horizontal" action="filtroEditorServlet" method="post">
                    <div class="modal-body">
                        <div class="well">							
                            <div class="form-group">
                                <label for="filter" class="col-sm-2 control-label">ID</label>
                                <div class="col-sm-10">						
                                    <input type="text" id="filterID" name="filterID" class="form-control" readonly="readonly">
                                </div>
                            </div>					
                            <div class="form-group">
                                <label for="filter" class="col-sm-2 control-label">Nome</label>
                                <div class="col-sm-10">
                                    <input type="text" id="filterName" name="filterName" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="filter" class="col-sm-2 control-label">EPL</label>
                                <div class="col-sm-10">
                                    <textarea type="text" id="filterEPL" name="filterEPL" class="form-control"rows="15"></textarea>
                                </div>
                            </div>		
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" id="exclusionButton" class="btn btn-danger" name="submitButton" value="excluir">Excluir</button>
                        <button type="submit" class="btn btn-primary" name="submitButton" value="editar">Salvar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal Confirm Activation -->
    <div class="modal fade" id="toggleFilterModal" tabindex="-1" role="dialog" aria-labelledby="ModalActivationFilter">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="ModalActivationFilter">Confirmar Mudança</h4>
                </div>
                <form role="form" action="filtroToggleServlet" method="post">
                    <div class="modal-body">
                        <div id="textToggle">							
                        </div>
                        <input name="hiddenIdFilter" id="hiddenIdFilter" type="hidden" value="">
                        <input name="hiddenStatusFilter" id="hiddenStatusFilter" type="hidden" value="">
                    </div>
                    <div class="modal-footer">				
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Confirmar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal New Filter Simple -->
    <div class="modal fade" id="newFilterSimple" tabindex="-1" role="dialog" aria-labelledby="ModalEditFilter">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="ModalNewFilterSimple">Novo Filtro</h4>
                </div>
                <form role="form" class="form-horizontal" action="filtroNovoServlet" method="post">
                    <div class="modal-body">
                        <div class="well">
                            <div class="form-group">
                                <label for="filter" class="col-sm-2 control-label">Nome</label>
                                <div class="col-sm-10">
                                    <input type="text" id="newFilterName" name="newFilterName" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="filter" class="col-sm-2 control-label">Regra</label>
                                <div class="col-sm-10">
                                    <textarea type="text" id="newfilterRegra" name="newFilterRegra" class="form-control"rows="15"></textarea>
                                </div>
                            </div>		
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Salvar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal Confirm Delete -->
    <div class="modal fade" id="deleteFilterModal" tabindex="-1" role="dialog" aria-labelledby="ModalDeleteFilter">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="ModalDeleteFilter">Confirmar Exclusão</h4>
                </div>
                <form role="form" action="filtroDeleteServlet" method="post">
                    <div class="modal-body">
                        <div id="textDelete">							
                        </div>
                        <input name="hiddenIdDeleteFilter" id="hiddenIdDeleteFilter" type="hidden" value="">
                    </div>
                    <div class="modal-footer">				
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-danger" name="submitButton" value="excluir">Excluir</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


</div>

